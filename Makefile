# vim: tabstop=8 noexpandtab

DESTDIR =
PREFIX  = /usr/local
MANDIR  = $(PREFIX)/share/man

CC      = gcc
CFLAGS  = -Wall -ansi -pedantic 
CFLAGS += -Wno-long-long
LFLAGS  = 

INC     = bits.h
SRC     = bits.c bitstream.c

OBJ     = $(SRC:.c=.o)
BIN     = example
LIB     = libbits

default: all

all:	$(LIB) $(BIN) tsx_bit_extract.so

$(BIN): $(LIB)
	$(CC) $(CFLAGS) -o $@ main.c $(LIB).a $(LFLAGS)

$(LIB): $(OBJ)
	ar rcs $(LIB).a $(OBJ)

clean:
	rm -f $(OBJ) $(BIN) $(LIB).a

test: $(BIN)
	./$(BIN)
	@[ $$? = 0 ] && echo "PASSED OK."

install: $(LIB)
	install -v -m 0644 $(LIB).a $(DEST)/lib
	install -v -m 0644 $(INC)   $(DEST)/include

install-man:
	install -d $(DESTDIR)$(MANDIR)/man3
	install -m 0644 bits.3 $(DESTDIR)$(MANDIR)/man3

uninstall:
	rm -f $(DEST)/lib/$(LIB).a
	rm -f $(DEST)/include/$(INC)

tsx_bit_extract.so: tsx_bit_extract.c 
	$(CC) -shared -Wall -fPIC -DUSE_DL=1 -I../tinyscheme-1.40 -o $@ $(DEBUG) $<

.PHONY: default clean test all install install-man uninstall 

