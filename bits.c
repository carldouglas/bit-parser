/*
 * Functions to read and write arbitrary bit segments from an array of octets.
 *
 * Copyright (C) 2011  A. Carl Douglas <carl.douglas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/* vim: softtabstop=2 shiftwidth=2 expandtab  */
#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>
#include <string.h>

unsigned long long bitread(unsigned char *bits, unsigned int offset, unsigned int length) {
  unsigned int index    = 0;
  unsigned long long value   = 0;

  for (index = 0; index < length; index++) {
    unsigned int byte_offset = ((offset + index) / 8);
    unsigned int bit_offset  = 7 - ((offset + index) % 8);
    unsigned long long b     = 0;
    b = (bits[byte_offset] & (1 << (bit_offset))) >> bit_offset;
    value |= (unsigned long long)(b << (length - 1 - index)); 
  }
  return value;
}

unsigned int bitscanf(unsigned char *bits, const char *fmt, ...) {
  va_list ap;
  unsigned int offset = 0, length = 0; 
  unsigned long long *value = 0;
  va_start(ap, fmt);
  while(*fmt)
    switch(*fmt++) {
      case '%':
        length = 0;
        while(isdigit(*fmt)) { length *= 10; length += *fmt - '0'; fmt++; }
        break;
      case ':':
        offset = 0;
        while(isdigit(*fmt)) { offset *= 10; offset += *fmt - '0'; fmt++; }
        break;
      default:
        value = va_arg(ap, unsigned long long *);
        if (value) *value = bitread(bits, offset, length);
        offset += length;
        break;
    }
  va_end(ap);
  return offset;
}

unsigned int readbits_str(unsigned char *bits, char *fmt, char *out, int out_len) {
  unsigned int offset = 0, length = 0; 
  unsigned long long value = 0;
  char name[32];
  char buffer[80];
  char *p;
  name[0] = buffer[0] = '\0';
  while(*fmt)
    switch(*fmt++) {
      case '%':
        length = 0;
        while(isdigit(*fmt)) { length *= 10; length += *fmt - '0'; fmt++; }
        break;
      case ':':
        offset = 0;
        while(isdigit(*fmt)) { offset *= 10; offset += *fmt - '0'; fmt++; }
        break;
      case '=':
        p = name;
        while(*fmt != 0x1e) { *p++ = *fmt++; }
        *p++ = '\0';
        break;
      case 'x':
        value = bitread(bits, offset, length);
        sprintf(buffer, "0x%llx", value);
        if (strlen(out) > 0) strcat(out, "&");
        if (strlen(name)> 0) strcat(out, name);
        if (strlen(name)> 0) strcat(out, "=");
        strcat(out, buffer);
        offset += length;
        buffer[0] = name[0] = '\0';
        break;
      case 'd':
        value = bitread(bits, offset, length);
        sprintf(buffer, "%lld", value);
        if (strlen(out) > 0) strcat(out, "&");
        if (strlen(name)> 0) strcat(out, name);
        if (strlen(name)> 0) strcat(out, "=");
        strcat(out, buffer);
        offset += length;
        buffer[0] = name[0] = '\0';
        break;
    }
  return offset;
}

void bitwrite(unsigned char *bits, unsigned int offset, unsigned int length, unsigned long long value) {
  unsigned int index    = 0;
  unsigned int bit      = length - 1;
  for (index = 0; index < length; index++) {
    unsigned int byte_offset = ((offset + index) / 8);
    unsigned int bit_offset  = 7 - ((offset + index) % 8);
    unsigned long long b     = 0;
    b = value & (1 << bit);
    b = b >> (bit);
    if (b == 0) bits[byte_offset] &= ~(1 << bit_offset);
    else        bits[byte_offset] |= (b << bit_offset); 
    bit--;
  }
}

unsigned int bitprintf(unsigned char *bits, char *fmt, ...) {
  va_list ap;
  unsigned int offset = 0, length = 0; 
  unsigned long long value = 0;
  va_start(ap, fmt);
  while(*fmt)
    switch(*fmt++) {
      case '%':
        length = 0;
        while(isdigit(*fmt)) { length *= 10; length += *fmt - '0'; fmt++; }
        break;
      case ':':
        offset = 0;
        while(isdigit(*fmt)) { offset *= 10; offset += *fmt - '0'; fmt++; }
        break;
      default:
        value = va_arg(ap, unsigned long long);
        bitwrite(bits, offset, length, value);
        offset += length;
        break;
    }
  va_end(ap);
  return offset;
}

