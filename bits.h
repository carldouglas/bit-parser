/*
 * Functions to read and write arbitrary bit segments from an array of octets.
 *
 * Copyright (C) 2011  A. Carl Douglas <carl.douglas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/* vim: softtabstop=2 shiftwidth=2 expandtab  */

#ifdef __cplusplus
extern "C" {
#endif

/*
 * read {length} number of bits from the octet stream {bits}, 
 * starting at {offset} number of bits
 */
unsigned long long bitread(
    unsigned char *bits, 
    unsigned int offset, 
    unsigned int length
    );

unsigned int bitscanf(
    unsigned char *bits, 
    const char *fmt, ...
    );

/*
 * write {length} number of bits from {value} to the octet stream {bits}, 
 * starting at {offset} number of bits
 */
void bitwrite(
    unsigned char *bits, 
    unsigned int offset, 
    unsigned int length, 
    unsigned long long value
    );

unsigned int bitprintf(
    unsigned char *bits, 
    char *fmt, ...);

/*** Experimental ***/
unsigned int readbits_str(
    unsigned char *bits, 
    char *fmt, 
    char *out, 
    int out_len);

#ifdef __cplusplus
}
#endif

