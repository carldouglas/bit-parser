(load-extension "tsx_bit_extract")

(define octet "\x01\xf0\x3f\x0f")

(define extract-tests 
  (list 
               `(#x01f0 . ,(bit-extract octet 0 16))
               `(#x0003 . ,(bit-extract octet 1  8))
               `(#x00f8 . ,(bit-extract octet 7  8))
               `(#x03c0 . ,(bit-extract octet 8 10))
               `(#x0f03 . ,(bit-extract octet 8 12))
               `(#x07   . ,(bit-extract octet 8 3))
               `(#x01f03f0f . ,(bit-extract octet 0 32))
               ;`("\xf0\x3f" . ,(bit-pack (make-string 3) 0 16))
               ))

(define extract-test 
  (lambda (x) 
    (display (if (= (car x) (cdr x)) "PASS\n" "FAIL\n" ))))

(for-each extract-test extract-tests) 

