/*
 * Functions to read and write arbitrary bit segments from an array of octets.
 *
 * Copyright (C) 2011  A. Carl Douglas <carl.douglas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/* vim: softtabstop=2 shiftwidth=2 expandtab  */

/* gcc -o bitstream bitstream.c -DMAIN */
#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>
#include <string.h>

struct bit_stream {
  FILE *fp;
  unsigned octet;
  unsigned offset;
};

void bit_stream_init(struct bit_stream *st, FILE *fp) {
  st->fp = fp;
  st->octet = 0;
  st->offset = 0;
}

unsigned long long 
bit_stream_read
(struct bit_stream *st, unsigned int length) {
  unsigned i = 0;
  unsigned long long value   = 0;
  unsigned int bit;
  for (i = 0; i < length; i++, st->offset++) {
    if ((st->offset % 8) == 0) {
      st->octet = fgetc(st->fp);
    }
    bit = (st->octet & (1 << (7 - (st->offset % 8)))) >> (7 - (st->offset % 8));
    value |= (unsigned long long) (bit << (length - 1 - i));
  }
  return value;
}

void 
bit_stream_write
(struct bit_stream *st, unsigned int length, unsigned long long value) {
  unsigned i = 0;
  unsigned int bit;
  for (i = 0; i < length; i++, st->offset++) {
    bit = value & (1 << (length - 1 - i));
    if ((st->offset % 8) == 0) {
    }
  }
}

#if MAIN
int main(int argc, char *argv[]) {
  struct bit_stream st1;
  unsigned long long v[16];
  FILE *fp = fopen(argv[1], "rb");
  bit_stream_init(&st1, fp);
m0:
  v[0] =  bit_stream_read(&st1, 3);
  v[1] =  bit_stream_read(&st1, 3);
  v[2] =  bit_stream_read(&st1, 10);
  v[3] =  bit_stream_read(&st1, 32);
  v[4] =  bit_stream_read(&st1, 32);
  printf ("--------------" __DATE__ "  " __TIME__ "\n");
  printf ("reserved: %lld\n", v[0]);
  printf ("version:  %lld\n", v[1]);
  printf ("type:     %lld\n", v[2]);
  printf ("length:   %lld\n", v[3]);
  printf ("id:       %lld\n", v[4]);
p0:
  v[5] = bit_stream_read(&st1, 1);
  if (feof(st1.fp)) goto f0;
  if (v[5] == 1) goto tv0;
  else goto tlv0;
tv0:
  v[6] = bit_stream_read(&st1, 7);
  printf ("  type:     %lld\n", v[6]);
  switch (v[6]) {
    case 13: 
      { int i;
        printf ("  epc 96: ");
        for (i = 0; i < 12; i++) {
          unsigned long long x = bit_stream_read(&st1, 8);
          printf ("%2X", (unsigned int) x);
        }
        printf ("\n");
      }
  }
  goto p0;
tlv0:
  v[7] = bit_stream_read(&st1, 5);
  printf ("  reserved: %lld\n", v[7]);
  v[8] = bit_stream_read(&st1, 10);
  printf ("  type:     %lld\n", v[8]);
  v[9] = bit_stream_read(&st1, 16);
  printf ("  length:   %lld\n", v[9]);
  goto p0;
f0:
  printf ("--------------" __DATE__ "  " __TIME__ "\n");
  fclose(fp);
  return 0;
}
#endif

