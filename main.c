/* vim: softtabstop=2 shiftwidth=2 expandtab  */
#include <stdio.h>
#include <assert.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "bits.h"

int main(int argc, char *argv[]) {
  unsigned char ip_packet[32] = {
    0x45, 0x00, 0x00, 0x48, 0xaa, 0xcd, 0x00, 0x00, 0xff, 0x11, 0x8c, 0x22, 0xc0, 0xa8, 0x01, 0x66,
    0xc0, 0xa8, 0x01, 0xfe, 0xe6, 0xf0, 0x00, 0x35, 0x00, 0x34, 0xa1, 0x37, 0x2e, 0xc3, 0x01, 0x00
  };
  unsigned char a[4] = { 0x01, 0xf0, 0x3f, 0x0f }; /* 01f03f0f */
  unsigned char b[4] = { 0x00, 0x00, 0x00, 0x00 };
  char out[80];
  unsigned long v = 0;

  unsigned long long version, header_length, dscp, ecn, total_length, identification, flags, offset,
                ttl, protocol, checksum, src, dst;
  struct sockaddr_in addr;

  printf("bitread\n");
  v = bitread(a, 0, 16); printf("%lu %08lx\n", v, v); assert(v == 0x01f0);
  v = bitread(a, 1, 8);  printf("%lu %08lx\n", v, v); assert(v == 0x3);
  v = bitread(a, 7, 8);  printf("%lu %08lx\n", v, v); assert(v == 0xf8);
  v = bitread(a, 8, 10); printf("%lu %08lx\n", v, v); assert(v == 0x3c0);
  v = bitread(a, 8, 12); printf("%lu %08lx\n", v, v); assert(v == 0xf03);
  v = bitread(a, 0, 32); printf("%lu %08lx\n", v, v); assert(v == 0x01f03f0f);
  v = bitread(a, 8, 3);  printf("%lu %08lx\n", v, v); assert(v == 0x07);

  printf("bitwrite\n");
  bitwrite(b, 0, 16, 0xf03f);   printf("%02x%02x%02x%02x\n", b[0], b[1], b[2], b[3]);
  assert((b[0] == 0xf0) && (b[1] == 0x3f) && (b[2] == 0x00) && (b[3] == 0x00));
  bitwrite(b, 1, 8, 0x02);      printf("%02x%02x%02x%02x\n", b[0], b[1], b[2], b[3]);
  assert((b[0] == 0x81) && (b[1] == 0x3f) && (b[2] == 0x00) && (b[3] == 0x00));
  bitwrite(b, 16, 16, 0x0101);  printf("%02x%02x%02x%02x\n", b[0], b[1], b[2], b[3]);
  assert((b[0] == 0x81) && (b[1] == 0x3f) && (b[2] == 0x01) && (b[3] == 0x01));

  printf("bitscanf\n");
  bitscanf(a, "%4:8b", &v);  printf("%02x%02x%02x%02x length: 4 offset: 8 value: %08lx\n", a[0], a[1], a[2], a[3], v);
  assert(v == 0x0f);
  bitscanf(a, "%3:5b", &v);  printf("%02x%02x%02x%02x length: 3 offset: 5 value: %08lx\n", a[0], a[1], a[2], a[3], v);
  assert(v == 0x01);
  bitscanf(a, "%8:16b", &v); printf("%02x%02x%02x%02x length: 8 offset: 16 value: %08lx\n", a[0], a[1], a[2], a[3], v);
  assert(v == 0x3f);

  bitscanf(ip_packet, "%4b%4b%6b%2b%16b%16b%3b%13b%8b%8b%16b%32b%32b", 
      &version, &header_length, &dscp, &ecn, &total_length, &identification, &flags, &offset,
      &ttl, &protocol, &checksum, &src, &dst);

  printf("ip packet:\n");
  printf(" version: %llu\n", version);
  printf(" header length: %llu\n", header_length);
  printf(" dscp: %llu\n", dscp);
  printf(" ecn: %llu\n", ecn);
  printf(" total length: %llu\n", total_length);
  printf(" identification: %llu\n", identification);
  printf(" flags: %llu\n", flags);
  printf(" offset: %llu\n", offset);
  printf(" ttl: %llu\n", ttl);
  printf(" protocol: %llu\n", protocol);
  printf(" checksum: %llu\n", checksum);
  addr.sin_addr.s_addr =  htonl(src);
  printf(" source: %s\n", inet_ntoa(addr.sin_addr));
  addr.sin_addr.s_addr =  htonl(dst);
  printf(" destination: %s\n", inet_ntoa(addr.sin_addr));

  printf("bitprintf\n"); 
  b[0] = 0x00; b[1] = 0x00; b[2] = 0x00; b[3] = 0x00;
  v = 0xf03f; bitprintf(b, "%16:0b", v);  printf("%02x%02x%02x%02x length: 16 offset: 0 value: %08lx\n", b[0], b[1], b[2], b[3], v);
  assert(v == 0xf03f);
  v = 0x0101; bitprintf(b, "%16:16b", v); printf("%02x%02x%02x%02x length: 16 offset: 16 value: %08lx\n", b[0], b[1], b[2], b[3], v);
  assert(v == 0x0101);
  v = 0x0202; bitprintf(b, "%16:16b", v); printf("%02x%02x%02x%02x length: 16 offset: 16 value: %08lx\n", b[0], b[1], b[2], b[3], v);
  assert(v == 0x0202);

  printf("readbits_str\n");
  out[0] = '\0';
  readbits_str(a, "%16x%16x", out, 80);
  printf("%s\n", out);
  out[0] = '\0';
  readbits_str(a, "%16=one\x1ex%16=two\x1ex", out, 80);
  printf("%s\n", out);

  return 0;
}

