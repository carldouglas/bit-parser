

#include "scheme-private.h"
#include "bits.h"

/* 
 * inspired by Guile scm_bit_extract (n, start, end) 
 */
pointer foreign_bit_extract(scheme * sc, pointer args)
{
  pointer sc_arg[3];

  unsigned char * octets;
  unsigned int offset;
  unsigned int length;
  unsigned int index;
  unsigned int bit;
  unsigned long value;

  if(args == sc->NIL)
    return sc->F;

  sc_arg[0] = sc->vptr->pair_car(args);
  args = sc->vptr->pair_cdr(args);
  sc_arg[1] = sc->vptr->pair_car(args);
  args = sc->vptr->pair_cdr(args);
  sc_arg[2] = sc->vptr->pair_car(args);
  args = sc->vptr->pair_cdr(args);

  if(!sc->vptr->is_number(sc_arg[1]) || !sc->vptr->is_number(sc_arg[2])) {
    return sc->F;
  }

  octets = (unsigned char *) sc->vptr->string_value(sc_arg[0]);
  offset = (unsigned int) sc->vptr->ivalue(sc_arg[1]);
  length = (unsigned int) sc->vptr->ivalue(sc_arg[2]);
  bit    = (unsigned int) (length - 1);
  value  = 0;
#if 0
  printf("DEBUG: %02X%02X%02X%02X \n", 
      octets[0], octets[1], octets[2], octets[3]);
  printf("DEBUG: offset %u length %u bit %u\n", offset, length, bit);
#endif
  for (index = 0; index < length; index++) {
    unsigned int byte_offset = ((offset + index) / 8);
    unsigned int bit_offset  = 7 - ((offset + index) % 8);
    unsigned long b     = 0;
    b = (octets[byte_offset] & (1 << (bit_offset)));
    b = b >> (bit_offset);
    value |= (unsigned long)(b << bit);
    bit--;
  }
#if 0
  printf("DEBUG: %lu\n", value);
#endif
  return (sc->vptr->mk_integer(sc,(long)value));
}

pointer foreign_bit_pack(scheme * sc, pointer args)
{
  pointer sc_arg[4];

  unsigned char * octets;
  unsigned int offset;
  unsigned int length;
  unsigned int index;
  unsigned int bit;
  unsigned long value;

  if(args == sc->NIL)
    return sc->F;
  sc_arg[0] = sc->vptr->pair_car(args);
  args = sc->vptr->pair_cdr(args);
  sc_arg[1] = sc->vptr->pair_car(args);
  args = sc->vptr->pair_cdr(args);
  sc_arg[2] = sc->vptr->pair_car(args);
  args = sc->vptr->pair_cdr(args);
  sc_arg[3] = sc->vptr->pair_car(args);
  args = sc->vptr->pair_cdr(args);

  if(!sc->vptr->is_number(sc_arg[1]) || !sc->vptr->is_number(sc_arg[2]) || !sc->vptr->is_number(sc_arg[3])) {
    return sc->F;
  }

  octets = (unsigned char *)sc->vptr->string_value(sc_arg[0]);
  offset = (unsigned int)sc->vptr->ivalue(sc_arg[1]);
  length = (unsigned int)sc->vptr->ivalue(sc_arg[2]);
  value  = (unsigned long)sc->vptr->ivalue(sc_arg[3]);
  bit    = length - 1;

  for (index = 0; index < length; index++) {
    unsigned int byte_offset = ((offset + index) / 8);
    unsigned int bit_offset  = 7 - ((offset + index) % 8);
    unsigned long long b     = 0;
    b = value & (1 << bit);
    b = b >> (bit);
    if (b == 0) octets[byte_offset] &= ~(1 << bit_offset);
    else        octets[byte_offset] |= (b << bit_offset);
    bit--;
  }
  return sc->T;
}

void init_tsx_bit_extract(scheme *sc) {

  sc->vptr->scheme_define(sc,sc->global_env,
              sc->vptr->mk_symbol(sc,"bit-extract"),
              sc->vptr->mk_foreign_func(sc, foreign_bit_extract));

  sc->vptr->scheme_define(sc,sc->global_env,
              sc->vptr->mk_symbol(sc,"bit-pack"),
              sc->vptr->mk_foreign_func(sc, foreign_bit_pack));
}

/* EOF */
